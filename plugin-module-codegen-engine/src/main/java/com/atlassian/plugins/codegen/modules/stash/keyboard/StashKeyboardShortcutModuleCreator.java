package com.atlassian.plugins.codegen.modules.stash.keyboard;

import com.atlassian.plugins.codegen.annotations.StashPluginModuleCreator;
import com.atlassian.plugins.codegen.modules.common.keyboard.AbstractKeyboardShortcutModuleCreator;

@StashPluginModuleCreator
public class StashKeyboardShortcutModuleCreator extends AbstractKeyboardShortcutModuleCreator<StashKeyboardShortcutProperties>
{
}
